FROM python:3.12

RUN pip install --no-cache --upgrade --break-system-packages \
    pip setuptools wheel

EXPOSE 7779/tcp
WORKDIR /app
CMD [ "python", "app.py" ]

COPY . /app

RUN pip install --no-cache --break-system-packages -r requirements.txt
