# graphql-api-fuzzing-example

This GraphQL example performs a dynamic-analysis security test
against a Python application that exposes a basic GraphQL endpoint.

In this configuration, GitLab DAST API performs an introspection query
against the endpoint to pull the schema and automatically test every
query and mutation that has input arguments.
